#include <iostream>
#include <vector>
#include <memory>
#include <random>
#include <stdexcept>
#include "student.hxx"
#include "manager.hxx"

using namespace ribomation::persons;
using std::cout;
using std::vector;
using std::string;
using std::unique_ptr;
using std::uniform_int_distribution;

auto create() -> unique_ptr<Person> {
    static std::random_device r;
    static auto names = vector<string>{
        "Anna", "Adam", "Berit", "Bertil", "Carin", "Carl"
    };
    static auto depts = vector<string>{
        "development", "finance", "marketing"
    };
    auto type = uniform_int_distribution<unsigned>{1, 3};
    auto id   = uniform_int_distribution<unsigned>{1111, 9999};
    auto pick = [](vector<string> const& data) -> string {
        auto idx = uniform_int_distribution<size_t>{0, data.size()-1};
        return data[idx(r)];
    };

    Person* ptr;
    switch (type(r)) {
        case 1: ptr = new Student{pick(names), id(r)}; break;
        case 2: ptr = new Employee{pick(names), id(r)}; break;
        case 3: ptr = new Manager{pick(names), id(r), pick(depts)}; break;
        default: throw std::overflow_error{"invalid type index"};
    }
    return unique_ptr<Person>{ptr};
}

auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
    return os << p.to_string();
}

int main(int argc, char** argv) {
    auto N = argc==1 ? 10 : std::stoi(argv[1]);
    auto objs = vector<unique_ptr<Person>>{};
    objs.reserve(N);
    
    while (N-- > 0) objs.push_back(create());
    for (auto& p : objs)  cout << *p << "\n";
}



