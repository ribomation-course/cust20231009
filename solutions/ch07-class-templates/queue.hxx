#pragma once
#include <stdexcept>

namespace ribomation::data {

    template<typename ElemType, unsigned CAPACITY>
    class Queue {
        ElemType storage[CAPACITY]{};
        unsigned putIdx = 0;
        unsigned getIdx = 0;
        unsigned size_ = 0;
    public:
        auto size() const { return size_; }
        auto capacity() const { return CAPACITY; }
        auto empty() const { return size() == 0; }
        auto full() const { return size() == capacity(); }

        void put(ElemType x) {
            if (full()) throw std::overflow_error{"full"};
            storage[putIdx] = x;
            putIdx = (putIdx + 1) % capacity();
            ++size_;
        }

        ElemType get() {
            if (empty()) throw std::underflow_error{"empty"};
            auto x = storage[getIdx];
            getIdx = (getIdx + 1) % capacity();
            --size_;
            return x;
        }
    };

}