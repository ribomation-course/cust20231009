#include <iostream>
#include <string>
#include <utility>
#include <functional>
#include "queue.hxx"

namespace rm = ribomation::data;
using namespace std::string_literals;

//D.R.Y. - Don't Repeat Yourself
template<typename ElemType, unsigned CAPACITY>
void drain(rm::Queue<ElemType, CAPACITY>& q) {
    while (!q.empty()) {
        std::cout << q.get() << ' ';
    }
    std::cout << '\n';
}

template<typename ElemType, unsigned CAPACITY>
void populate(rm::Queue<ElemType, CAPACITY>& q, std::function<ElemType(unsigned)> mkData) {
    for (auto k = 1U; !q.full(); ++k) {
        q.put(mkData(k));
    }
}

struct Person {
    std::string name{};
    unsigned age{};

    Person() = default;

    Person(std::string name, unsigned age) : name{std::move(name)}, age{age} {}

    friend auto operator<<(std::ostream& os, const Person& p) -> std::ostream& {
        return os << "Person{" << p.name << ", " << p.age << '}';
    }
};

int main() {
    {
        auto q = rm::Queue<int, 16>();
        populate<int, 16>(q, [](auto k) { return k; });
        drain(q);
    }
    {
        auto q = rm::Queue<std::string, 8>();
        populate<std::string, 8>(q, [](auto k) { return "string-"s + std::to_string(k); });
        drain(q);
    }
    {
        auto q = rm::Queue<Person, 4>();
        populate<Person, 4>(q, [](auto k) { return Person{"Nisse-"s + std::to_string(k), 20 + 5 * k}; });
        drain(q);
    }
    {
        auto str = "hello"s  + std::string{"world"}; //char[6] ~ char* = decay
    }
}


