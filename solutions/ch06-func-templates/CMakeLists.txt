cmake_minimum_required(VERSION 3.25)
project(ch06_func_templates)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(equals equals.cxx)
target_compile_options(equals PRIVATE ${WARN})

add_executable(map-reduce map-reduce.cxx)
target_compile_options(map-reduce PRIVATE ${WARN})
