#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
using namespace std::literals;
using std::cout;
using std::string;


template<typename Iterator, typename MapFn, typename ReduceFn>
auto map_reduce(Iterator first, Iterator last, MapFn mapper, ReduceFn reducer) {
    auto result = decltype(mapper(*first)){mapper(*first)};
    for (++first; first != last; ++first) {
        result = reducer(result, mapper(*first)); // sum = sum + fn(n)
    }
    return result;
}

auto to_upper(string s) {
    std::transform(s.cbegin(), s.cend(), s.begin(), [](auto ch) { return ::toupper(ch); });
    return s;
}

int main() {
    {
        int num[] = {1, 2, 3, 4};
        auto result = map_reduce(std::begin(num), std::end(num),
                                 [](int n) { return n * n; },
                                 [](int sum, int n) { return sum + n; });
        cout << "result: " << result << "\n";
        cout << "check : " << (1 + 4 + 9 + 16) << "\n";
    }

    cout << "----\n";
    {
        auto words = std::vector<string>{"one"s, "two"s, "three"s};
        auto result = map_reduce(std::begin(words), std::end(words),
                                 [](auto const& s) { return s.size(); },
                                 [](auto sum, auto n) { return sum + n; });
        cout << "result: " << result << "\n";
        cout << "check : " << (3 + 3 + 5) << "\n";
    }

    cout << "----\n";
    {
        auto words = std::vector<string>{"one"s, "two"s, "three"s};
        auto result = map_reduce(std::begin(words), std::end(words),
                                 [](string s){ return to_upper(s); },
                                 [](string sum, string s){ return sum + "/"s + s; }
                                 );
        cout << "result: " << result << "\n";
    }
    //printf("%s: %d\n", "func", 42)
}

