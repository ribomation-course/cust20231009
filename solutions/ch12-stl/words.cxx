#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

using std::cout;
using std::string;
namespace r = std::ranges;
using namespace std::string_literals;

int main() {
    auto words = std::vector<string>{};
    words.reserve(100);

    cout << "-- load --\n";
    for (string w; std::cin >> w;) words.emplace_back(w);
    cout << "loaded " << words.size() << " words\n";

    cout << "-- print --\n";
    r::for_each(words, [](auto&& w) { cout << w << " "; });
    cout << "\n";

    {
        cout << "-- total size --\n";
        auto sizes = std::vector<unsigned>{};
        r::for_each(words, [&sizes](auto&& w) { sizes.push_back(w.size()); });
        cout << "total: " << std::accumulate(sizes.begin(), sizes.end(), 0U) << " chars\n";
    }

    cout << "-- insert first --\n";
    words.insert(std::begin(words), "THIS IS THE FIRST"s);

    cout << "-- print & drop --\n";
    while (!words.empty()) {
        auto w = words.back();
        words.pop_back();
        cout << "[" << w << "] ";
    }
    cout << "\n";
}
