#pragma once
#include <iosfwd>

namespace ribomation::length {
    using LengthType = long double;
    using LengthTypeInt = unsigned long long;

    struct Length {
        const LengthType value;
        constexpr explicit Length(const LengthType value) : value{value} {}
        auto operator <=>(const Length&) const = default;
    };

    constexpr auto operator "" _m(const LengthType amount) {
        return Length{amount};
    }
    constexpr auto operator "" _m(const LengthTypeInt amount) {
        return Length{static_cast<LengthType>(amount)};
    }

    constexpr auto operator "" _km(const LengthType amount) {
        return Length{1000 * amount};
    }
    constexpr auto operator "" _km(const LengthTypeInt amount) {
        return Length{1000.0 * static_cast<LengthType>(amount)};
    }

    constexpr auto operator "" _mi(const LengthType amount) {
        return Length{1'609.344 * amount};
    }
    constexpr auto operator "" _mi(const LengthTypeInt amount) {
        return Length{1'609.344 * static_cast<LengthType>(amount)};
    }

    constexpr auto operator "" _ya(const LengthType amount) {
        return Length{0.9144 * amount};
    }
    constexpr auto operator "" _ya(const LengthTypeInt amount) {
        return Length{0.9144 * static_cast<LengthType>(amount)};
    }


    constexpr auto operator +(const Length& lhs, const Length& rhs) {
        return Length{lhs.value + rhs.value};
    }

    constexpr auto operator -(const Length& lhs, const Length& rhs) {
        return Length{lhs.value - rhs.value};
    }

    inline std::ostream& operator <<(std::ostream& os, const Length& w) {
        return os << w.value << " metres";
    }

}
