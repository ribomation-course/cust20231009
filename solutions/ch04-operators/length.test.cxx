#include <iostream>
#include <iomanip>
#include "length.hxx"
using std::cout;
using std::fixed;
using std::setprecision;
using namespace ribomation::length;

int main() {
    {
        auto distance = 2_km + 12.5_m - 0.5_mi + 31_ya;
        cout << "distance: " << fixed << setprecision(3) << distance << "\n";
    }

    {
        auto d1 = 2_km;
        auto d2 = 2.5_km;
        cout << "d1: " << d1 << "\n";
        cout << "d2: " << d2 << "\n";
        cout << std::boolalpha;
        cout << "d1 == d2: " << (d1 == d2) << "\n";
        cout << "d1 != d2: " << (d1 != d2) << "\n";
        cout << "d1 < d2: " << (d1 < d2) << "\n";
        cout << "d1 > d2: " << (d1 > d2) << "\n";
    }
}
