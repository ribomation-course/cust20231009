#include <sstream>
#include <cmath>
#include "math-lib.hxx"

namespace ribomation::math {
    using namespace std::string_literals;

    auto log_2(double x) -> double {
        auto result = std::log2(x);
        if (std::isfinite(result)) {
            return result;
        }

        if (std::isnan(result)) {
            auto buf = std::ostringstream{};
            buf << "log2("<<x<<") --> NaN";
            throw MathError{buf.str()};
        }

        if (std::isinf(result)) {
            auto buf = std::ostringstream{};
            buf << "log2("<<x<<") --> oo";
            throw MathError{buf.str()};
        }

        throw MathError{"log2: unknown error"s};
    }

}
