#pragma once
#include <stdexcept>
#include <string>

namespace ribomation::math {
    using std::runtime_error;
    using std::string;

    struct MathError : runtime_error {
        explicit MathError(const string& msg) : runtime_error{msg} {}
    };

    auto log_2(double x) -> double;
}
