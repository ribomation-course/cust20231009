#include <iostream>
#include "math-lib.hxx"

void invoke(double x) {
    using namespace ribomation::math;
    using std::cout;
    try {
        cout << "log[2](" << x << ") = " << log_2(x) << "\n";
    } catch (const MathError& err) {
        cout << "ERROR: " << err.what() << "\n";
    } catch (const std::exception& err) {
        cout << "Unknown: " << err.what() << "\n";
    }
}

int main() {
    invoke(1024);
    invoke(0);
    invoke(-17);
}
