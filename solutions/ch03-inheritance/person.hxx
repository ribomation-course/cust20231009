#pragma once

#include <string>

namespace ribomation::persons {
    using std::string;
    using namespace std::string_literals;

    struct Person {
        explicit Person(string name_) : name{std::move(name_)} {}

        virtual ~Person() = default;
        Person(Person const&) = delete;
        Person(Person &&) noexcept = delete;
        auto operator=(Person const&) -> Person& = delete;
        auto operator=(Person &&) noexcept -> Person&  = delete;

        [[nodiscard]] virtual auto to_string() const -> string {
            return "Person{name="s + name + "}"s;
        }

    private:
        string name{};
    };
}