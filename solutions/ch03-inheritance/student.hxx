#pragma once
#include "person.hxx"

namespace ribomation::persons {

    struct Student : Person {
        Student(string name_, unsigned id_) : super{std::move(name_)}, id{id_} {}
        [[nodiscard]] auto to_string() const -> string override {
            return "Student{id="s + std::to_string(id) +", "s+ super::to_string() + "}"s;
        }

    private:
        unsigned id;
        using super = Person;
    };

}