#pragma once

#include "person.hxx"

namespace ribomation::persons {

    struct Manager : Employee {
        Manager(string name_, unsigned id_, string dept_)
                : super{std::move(name_), id_}, department{std::move(dept_)} {}

        [[nodiscard]] auto to_string() const -> string override {
            return "Manager{dept="s + department + ", "s + super::to_string() + "}"s;
        }

    private:
        string department;
        using super = Employee;
    };

}