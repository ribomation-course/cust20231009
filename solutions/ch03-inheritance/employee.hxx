#pragma once
#include "person.hxx"

namespace ribomation::persons {

    struct Employee : Person {
        Employee(string name_, unsigned id_) : super{std::move(name_)}, id{id_} {}
        [[nodiscard]] auto to_string() const -> string override {
            return "Employee{id="s + std::to_string(id) + ", "s + super::to_string() + "}"s;
        }

    private:
        unsigned id;
        using super = Person;
    };
}