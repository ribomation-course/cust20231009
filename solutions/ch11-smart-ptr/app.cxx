#include <iostream>
#include "use-unique-ptr.hxx"
#include "use-shared-ptr.hxx"

int main() {
    use_unique_ptr();
    std::cout << "------\n";
    use_shared_ptr();
}
