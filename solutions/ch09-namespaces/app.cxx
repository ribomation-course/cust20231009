#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include "lib.hxx"

int main() {
    using std::cout;
    using namespace ribomation::lib;
    cout << func1() << "\n";
    cout << func2() << "\n";
    cout << func3() << "\n";

    {
        int arr[] = {1, 2, 3, 4, 5};
        std::transform(arr, arr + 5, arr, [](int x) { return x * x; });
        for (auto n: arr) cout << n << " ";
        cout << "\n";
        auto sum = std::accumulate(arr, arr + 5, 0, [](int s, int n) { return s + n; });
        cout << "sum: " << sum << "\n";
        int arr2[5];
        std::copy(arr, arr + 5, arr2);
        for (auto n: arr2) cout << n << " ";
        cout << "\n";
    }

    {
        auto v = std::vector<int>{1, 2, 3, 4, 5};
        std::for_each(v.begin(), v.end(), [&v](int n) {
            v.push_back(n * n);
        });
        for (auto n : v) cout << n << " ";
        cout << "\n";
    }
}
