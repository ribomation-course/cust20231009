#pragma once
#include <string>

namespace ribomation::lib {
    auto func1() -> std::string;
    auto func2() -> std::string;
    auto func3() -> std::string;
}
