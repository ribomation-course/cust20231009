#include <iostream>
#include <tuple>
#include <map>

auto fib(int n) {
    if (n < 0) return 0;
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

auto compute(int n) {
    return std::make_tuple(n, fib(n));
}

auto populate(int n) {
    auto result = std::map<int, unsigned>{};
    for (auto k = 1; k <= n; ++k) {
        auto [a, r] = compute(k);
        result[a] = r; //result.operator[](a) = r --> std::pair<int,unsigned> p.first/second
    }
    return result;
}

int main() {
    auto const N = 10;
    {
        auto r = fib(N);
        std::cout << "fib(" << N << ") = " << r << "\n";
    }
    {
        auto [a, r] = compute(N);
        std::cout << "fib(" << a << ") = " << r << "\n";
    }
    {
        auto T = compute(N);
        std::cout << "fib(" << std::get<0>(T) << ") = " << std::get<1>(T) << "\n";
    }
    {
        int a; unsigned r;
        std::tie(a, r) = compute(N);
        std::cout << "fib(" << a << ") = " << r << "\n";
    }
    auto ptr = new int{42};
    std::cout << *ptr;

    std::cout << "----\n";
    {
        for (auto [a, r]: populate(N)) {
            std::cout << "fib(" << a << ") = " << r << "\n";
        }
    }

    delete ptr;
}
