#include <iostream>
#include <functional>

using std::cout;
using std::function;

template<typename T>
void map(T* arr, unsigned N, function<T(T)> fn) {
    for (auto k = 0U; k < N; ++k) *(arr+k) = fn( *(arr+k) );
}

template<typename T>
T reduce(T* arr, unsigned N, function<T(T, T)> fn) {
    T acc = arr[0];
    for (auto k = 1U; k < N; ++k) acc = fn(acc, arr[k]);
    return acc;
}

int main() {
    {
        int ints[] = {1, 2, 3, 4, 5};
        map<int>(ints, std::size(ints), [](int n) { return n * n; });
        cout << "ints: ";
        for (auto n: ints) cout << n << " ";
        cout << "\n";
    }
    {
        int ints[] = {1, 2, 3, 4, 5};
        auto prod = reduce<int>(ints, std::size(ints), [](int p, int n) { return p * n; });
        cout << "prod: " << prod << "\n";
    }
    {
        int ints[] = {1, 2, 3, 4, 5};
        cout << ints[2] << ", " << *(ints + 2) << ", " << 2[ints] << "\n";
    }
}
