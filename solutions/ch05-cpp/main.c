extern int printf(const char*, ...);
extern int atoi(const char*);

#define MAX(a, b)   (a >= b ? a : b)

int main(int n, char* args[]) {
    int x=42, y=64;
    if (n >= 2) x = atoi(args[1]);
    if (n >= 3) y = atoi(args[2]);

    printf("max(%d, %d) = ", x+1, y+1);
    {
        int t1 = ++x;
        int t2 = ++y;
        printf("%d\n", MAX(t1, t2));
    }
}
