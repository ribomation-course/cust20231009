#include <iostream>
#include <cstring>
using std::cout;

char* copystr(char const* str) { //::strdup
    if (str == nullptr) return nullptr;
    return ::strcpy(new char[::strlen(str) + 1], str);
}

class Person {
    char* name = nullptr;
    unsigned age = 0;
public:
    Person() {
        cout << "Person{} @ " << this << "\n";
    }
    Person(char const* name_, unsigned age_)
            : name{copystr(name_)}, age{age_} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }
    Person(Person const& rhs)
            : name{copystr(rhs.name)}, age{rhs.age} {
        cout << "Person{Person const&} @ " << this << "\n";
    }
    ~Person() {
        delete[] name;
        cout << "~Person{} @ " << this << "\n";
    }
    friend auto operator<<(std::ostream& os, Person const& rhs) -> std::ostream& {
        return os << "Person{" << rhs.name << ", " << rhs.age << "}";
    }
};

void func2(Person q) {
    cout << "[func2] q=" << q << "\n";
}
void func1(Person p) {
    cout << "[func1] p=" << p << "\n";
    func2(p);
}

int main() {
    cout << "[main] enter\n";
    {
        Person anna{"Anna Conda", 42};
        cout << "[main] anna=" << anna << "\n";
        func1(anna);
    }
    cout << "[main] exit\n";
}
