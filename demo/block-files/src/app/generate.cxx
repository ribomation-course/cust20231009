#include <iostream>
#include <string>
#include <random>
#include <filesystem>
#include "block-file.hxx"
#include "account.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;
using namespace ribomation::bank;
namespace fs = std::filesystem;

auto r = random_device{};  // /dev/random

auto nextAccno() -> string {
    auto nextLetter = uniform_int_distribution{'A', 'Z'};
    auto nextDigit  = uniform_int_distribution{'0', '9'};
    return ""s
           + nextLetter(r)
           + nextLetter(r)
           + nextLetter(r)
           + nextDigit(r)
           + nextDigit(r)
           + nextDigit(r);
}

auto nextBalance() -> int {
    auto nextValue = normal_distribution<float>{100, 75};
    return static_cast<int>(nextValue(r));
}

int main(int argc, char** argv) {
    auto const N    = (argc <= 1) ? 5 : stoi(argv[1]);
    auto const file = "account.db"s;
    if (fs::exists(file)) fs::remove(file);
    {
        auto      db = BlockFile<Account>{file};
        for (auto k  = 0; k < N; ++k) {
            db << Account{nextAccno(), nextBalance()};
        }
    }
    cout << "Generated " << N << " accounts in "
         << file << ", " << fs::file_size(file) << " chars" << endl;
}
