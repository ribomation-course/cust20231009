#pragma once

#include <string>
#include <algorithm>
#include <iosfwd>
#include <sstream>
#include <iomanip>

namespace ribomation {
    using namespace std;
    using namespace std::string_literals;

    template<unsigned N>
    class Number {
        char storage[N]{};
    public:
        Number() { value(0); }
        explicit Number(int val) { value(val); }

        void value(int val) {
            auto buf = ostringstream{};
            auto  w   = N;
            if (val < 0) { buf << '-'; --w; }
            buf << setfill('0') << setw(w) << abs(val);
            buf.str().copy(storage, N);
        }
        [[nodiscard]] int value() const {
            auto str = string{storage};
            return stoi(str);
        }

        operator int() const { return value(); }
        auto operator=(int val) -> Number<N>& {
            value(val);
            return *this;
        }

        friend auto operator<<(ostream& os, const Number<N>& t) -> ostream& {
            return os << t.value();
        }

        [[nodiscard]] const char* getStorage() const { //for testing only
            return storage;
        }
    };

}


