#pragma once
#include <string>
#include <algorithm>
#include <iosfwd>

namespace ribomation {
    using std::string;
    using std::fill_n;
    using std::find_first_of;
    using std::begin;
    using std::end;
    using std::ostream;

    template<unsigned N>
    class Text {
        char    storage[N]{};
    public:
        Text() { value(""); }
        explicit Text(string val) { value(val); }

        void value(const string& val) {
            fill_n(storage, N, '#');
            val.copy(storage, N);
        }
        [[nodiscard]] string value() const {
            auto str = string{begin(storage), end(storage)};
            auto pos = str.find_first_of('#');
            if (pos == string::npos) return str;
            return str.substr(0, pos);
        }

        operator string() const { return value(); }
        auto operator =(string val) -> Text<N>& {
            value(val);
            return *this;
        }

        friend auto operator <<(ostream& os, const Text<N>& t) -> ostream& {
            return os << t.value();
        }

        [[nodiscard]] const char* getStorage() const { //for testing only
            return storage;
        }
    };

}


