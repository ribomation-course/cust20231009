#include "catch.hpp"
#include "text.hxx"
#include "block-file.hxx"
#include <filesystem>

using namespace ribomation;
using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;

struct Dummy {
    Text<5> name{};
    Dummy() = default;
    Dummy(string const& name_) : name{name_} {}
};

auto const   dbFileName = fs::path{"./tst.db"s};
const string names[]    = {"one"s, "two"s, "three"s};

TEST_CASE("empty file", "[block-file]") {
    auto db = BlockFile<Dummy>{dbFileName};
    REQUIRE(fs::exists(dbFileName));
    REQUIRE(fs::file_size(dbFileName) == 0);
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("one append", "[block-file]") {
    {
        auto db    = BlockFile<Dummy>{dbFileName};
        auto dummy = Dummy{};
        dummy.name = "Dumbo Jumbo!";
        db << dummy;
    }
    REQUIRE(fs::file_size(dbFileName) == sizeof(Dummy));
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("three appends", "[block-file]") {
    {
        auto db = BlockFile<Dummy>{dbFileName};
        db << Dummy{"one"s};
        db << Dummy{"two"s};
        db << Dummy{"three"s};
    }
    REQUIRE(fs::file_size(dbFileName) == 3 * sizeof(Dummy));
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("size", "[block-file]") {
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        for (auto name : names) db << Dummy{name};
    }
    {
        auto db = BlockFile<Dummy>{dbFileName};
        REQUIRE(db.size() == size(names));
    }
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("for-each", "[block-file]") {
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        for (auto name : names) db << Dummy{name};
    }
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        auto      ix = 0U;
        for (auto blk : db) {
            REQUIRE(blk.name.value() == names[ix++]);
        }
    }
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("indexed read, using operator *", "[block-file]") {
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        for (auto name : names) db << Dummy{name};
    }
    {
        auto db  = BlockFile<Dummy>{dbFileName};
        auto blk = *db[1];
        REQUIRE(blk.name.value() == "two"s);
    }
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("indexed read, using type-conversion", "[block-file]") {
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        for (auto name : names) db << Dummy{name};
    }
    {
        auto  db  = BlockFile<Dummy>{dbFileName};
        Dummy blk = db[1];
        REQUIRE(blk.name.value() == "two"s);
    }
    REQUIRE(fs::remove(dbFileName));
}

TEST_CASE("indexed read/write", "[block-file]") {
    {
        auto      db = BlockFile<Dummy>{dbFileName};
        for (auto name : names) db << Dummy{name};
    }
    {
        auto db = BlockFile<Dummy>{dbFileName};
        REQUIRE(db[1].operator Dummy().name.value() == "two"s);
        db[1] = Dummy{"HEPP"s};
    }
    {
        auto db = BlockFile<Dummy>{dbFileName};
        REQUIRE(db[1].operator Dummy().name.value() == "HEPP"s);
    }
    REQUIRE(fs::remove(dbFileName));
}




